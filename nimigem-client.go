package main

import (
	"bitbucket.org/loemmet/go-gemini"
	"bufio"
	"bytes"
	"fmt"
	"github.com/AlecAivazis/survey"
	flag "github.com/spf13/pflag"
	"io"
	"io/ioutil"
	"net/url"
	"os"
	"regexp"
	"strings"
	"time"
)

var version = "0.1.2"

//nimigem upper request limit is 15kb which equates to approx ~10kb unencoded for typical
//western european language use when authored by humans.
var nimigemRequestMaxSize = 15 //in kb

var (
	payloadPath     = flag.StringP("payloadPath", "p", "", "Nimigem payload path, containing plain text. Otherwise uses stdin. For Nimigem requests only.\n")
	payloadMime     = flag.StringP("payloadMime", "m", "text/plain;charset=utf-8", "Nimigem payload Mime/media type. For Nimigem requests only.\n")
	outputPath      = flag.StringP("output", "o", "", "Gemini body output path to save to. For Gemini requests only\n")
	verFlag         = flag.BoolP("version", "v", false, "Find out what version of nimigem-client you're running.\n")
	quiet           = flag.BoolP("quiet", "q", false, "Don't show any messages, such as request and response previews.\n")
	insecure        = flag.BoolP("insecure", "i", false, "Don't check server certificates.\n")
	numPreviewLines = flag.IntP("lines", "l", 20, "Number of preview lines of Gemini text responses to show.\n")
	certPath        = flag.StringP("cert", "c", "", "Path to a PEM encoded TLS client certificate to be sent with the request.\n")
	keyPath         = flag.StringP("key", "k", "", "Path to a PEM encoded TLS key for the provided client cert.\n")
	payloadText     = flag.StringP("payloadText", "t", "", "Nimigem payload plain text. For Nimigem requests only.\n")
	extractEdit     = flag.BoolP("extractEdit", "e", false, "Extract Nimigem editor region, edit with system text editor, and submit to linked end point. For Gemini requests only.\n")

	certPEM []byte
	keyPEM  []byte
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func saveFile(contents []byte, path string) {
	d1 := contents
	err := ioutil.WriteFile(path, d1, 0644)
	check(err)
}

func readStdin() string {
	// based on https://flaviocopes.com/go-shell-pipes/
	reader := bufio.NewReader(os.Stdin) //default size is 4096 apparently
	var output []rune

	for {
		input, _, err := reader.ReadRune()
		if err != nil && err == io.EOF {
			break
		}
		output = append(output, input)
	}

	return string(output)
}

//getInput gets the content from the payloadPath or stdin
func getInput() ([]byte, error) {
	var result []byte

	info, err := os.Stdin.Stat()
	check(err)

	if info.Mode()&os.ModeNamedPipe != 0 {

		if *payloadPath != "" || *payloadText != "" {
			fatal("invalid flags: --payloadPath or ---payloadText values must be empty when piping to stdin")
		}
		// we have a pipe input
		result = []byte(readStdin())
	} else {
		if *payloadPath != "" {

			if *payloadText != "" {
				fatal("invalid flags: --payloadText value must be empty when passing a payloadPath")
			}

			//get the input file from the command line
			dat, err := ioutil.ReadFile(*payloadPath)
			check(err)
			result = dat
		} else {
			//no piped content and no payload path, use text passed on the command line, even if empty by default
			result = []byte(*payloadText)
		}
	}

	return result, nil
}

//ShowMessage prints a message to stderr, unless the quiet flag is activated
func ShowMessage(s string) {
	if !*quiet {
		fmt.Fprintf(os.Stderr, "%s\n", s)
	}
}

func fatal(format string, a ...interface{}) {
	if *quiet {
		//just output without extra indentation etc
		format = "Error: " + strings.TrimRight(format, "\n") + "\n"
	} else {
		format = "\nError:\n\t" + strings.TrimRight(format, "\n") + "\n\n"
	}
	fmt.Fprintf(os.Stderr, format, a...)

	os.Exit(1)
}

func fatalIsFile(name, path string) {
	fi, err := os.Stat(path)
	if err == nil {
		if fi.IsDir() {
			fatal("%s path points to a directory: %s", name, path)
		}
	} else if os.IsNotExist(err) {
		fatal("%s does not exist: %v", path, err)
	} else {
		// Some other error - permissions for example
		fatal("Couldn't access %s at %s: %v", name, path, err)
	}
}

//PreviewTextBody shows a preview of the text, truncated for ease of screen display
func PreviewTextBody(body string, label string) {
	ShowMessage("\tbody: \n")
	bodyLines := strings.Split(body, "\n")
	for i, line := range bodyLines {
		//show first n lines indented
		if i < *numPreviewLines {
			if len(line) > 70 {
				ShowMessage("\t│ " + line[0:68] + "...")
			} else {
				ShowMessage("\t│ " + line)
			}
		} else {
			ShowMessage("\t│ <" + label + " continues>...")
			break
		}
	}
}

func confirmProceed(prompt string) bool {
	result := false
	query := &survey.Confirm{
		Message: prompt,
	}

	survey.AskOne(query, &result)
	return result
}

func findNimigemEditRegion(geminiText string) (region string, nimigemUrl string, err error) {

	var useText = strings.Replace(geminiText, "\r\n", "\n", -1) //normalise line endings
	var lines = strings.Split(useText, "\n")
	var result = []string{}
	var inNimigemRegion = false
	var foundNimigemRegion = false
	nimigemUrl = ""
	region = ""


	const ZWSP = '\u200B'

	//find the first nimigem region and the subsequent nimigem URL if any
	for _, line := range lines {
		//fmt.Printf("examine line: %s\n", line)
		if strings.HasPrefix(line, "```✏️") {
			inNimigemRegion = true
			foundNimigemRegion = true
		} else if strings.HasPrefix(line, "```") {
			inNimigemRegion = false
		} else if inNimigemRegion {

			//check if it is a nimigem escaped preformatted line type, and unescape if so
			//by removing the ZWSP prefix
			if strings.HasPrefix(line, string(ZWSP) + "```") {
				result = append(result, line[3:])	//trim off ZWSP
			} else {
				result = append(result, line)	//some other line in the region - keep as is
			}
		} else {
			if foundNimigemRegion {
				//exit when we find the first subsequent nimigem url
				m := regexp.MustCompile(`=>\s*(nimigem://\S+)`)
				var matches = m.FindStringSubmatch(line)
				if len(matches) > 1 {
					nimigemUrl = matches[1]
					region = strings.Join(result, "\n")
					err = nil

					return
				}
			}
		}
	}

	//not found
	err = fmt.Errorf("cannot find Nimigem region in gemtext")
	return
}
func extractNimigemEdit(geminiText string) {
	content := ""
	var response *gemini.Response

	region, url, err := findNimigemEditRegion(geminiText)
	if err != nil {
		fmt.Printf("\nCould not find any Nimigem text region for extraction.\n")
		return
	}

	if confirmProceed("Edit extracted text from Nimigem region?") {
		prompt := &survey.Editor{
			Message:       "Press Enter to edit Nimigem text",
			FileName:      "*.txt",
			Default:       region,
			AppendDefault: true,
			HideDefault:   true,
			Help:          "This will launch an external editor where you can provide the text. \nSave and close the external editor to return.",
		}
		err = survey.AskOne(prompt, &content)

		if err != nil {
			fmt.Println(err.Error())
			return
		}
		// print the answers
		//fmt.Printf("you entered %s.\n" , content)

		if confirmProceed("Submit content to <" + url + "> ?") {
			//send to nimigem end point
			if len(certPEM) > 0 {
				response, err = gemini.SendWithCert(url, []byte(content), "text/plain", certPEM, keyPEM)
			} else {
				response, err = gemini.Send(url, []byte(content), "text/plain")
			}

			if err != nil {
				fmt.Println(err.Error())
				return
			}

			//confirm result - should be status 25 for success
			ShowMessage(fmt.Sprintf("\nNimigem response: \n\theader: %d %s\n", response.Status, response.Meta))
		}
	}

}

//main method processes command line arguments and retrieves content
func main() {
	var response *gemini.Response
	var err error
	var uri *url.URL

	flag.Parse()

	if *verFlag {
		fmt.Println("nimigem-client v" + version)
		return
	}

	if len(flag.Args()) == 0 {
		fmt.Println("usage: nimigem-client <flags> <url>\nSee --help for info on the available options.")
		return
	}

	passedUrl := flag.Args()[0]
	uri, err = url.Parse(passedUrl)
	check(err)

	if uri.Scheme != "gemini" && uri.Scheme != "nimigem" {
		fatal("supplied argument was not a gemini or nimigem url: %s. \nUsage: nimigem-client <flags> <url>", flag.Args()[0])
	}

	// from gemgetValidate cert and key files
	if *certPath != "" && *keyPath != "" {
		fatalIsFile("cert file", *certPath)
		fatalIsFile("key file", *keyPath)

		var err error
		certPEM, err = ioutil.ReadFile(*certPath)
		if err != nil {
			fatal("Cert file could not be read: %v", err)
		}
		keyPEM, err = ioutil.ReadFile(*keyPath)
		if err != nil {
			fatal("Key file could not be read: %v", err)
		}
	}

	//create a basic client
	Client := &gemini.Client{ConnectTimeout: 15 * time.Second}
	Client.Insecure = *insecure

	if uri.Scheme == "nimigem" {
		//get the payload from commandline or stdin

		payload, err := getInput()
		check(err)

		//show a preview of the payload if text based
		ShowMessage(fmt.Sprintf("\nNimigem request: \n\turl: %s", passedUrl))
		if strings.Contains(*payloadMime, "text/") {
			PreviewTextBody(string(payload), "payload")
		} else {
			ShowMessage("\tbody: <some binary " + *payloadMime + " was sent as a payload>...")
		}

		if len(certPEM) > 0 {
			response, err = Client.SendWithCert(passedUrl, payload, *payloadMime, certPEM, keyPEM)
		} else {
			response, err = Client.Send(passedUrl, payload, *payloadMime)
		}
		if err != nil {
			fatal("error on posting Nimigem content: %s ", err)
			return
		}

		ShowMessage(fmt.Sprintf("\nNimigem response: \n\theader: %d %s\n", response.Status, response.Meta))

	} else {

		ShowMessage(fmt.Sprintf("\nGemini request: \n\turl: %s", passedUrl))

		if len(certPEM) > 0 {
			response, err = Client.FetchWithCert(passedUrl, certPEM, keyPEM)
		} else {
			response, err = Client.Fetch(passedUrl)
		}

		if err != nil {
			fatal("cannot fetch Gemini resource: %s ", err)
			return
		}

		ShowMessage(fmt.Sprintf("\nGemini response: \n\theader: %d %s", response.Status, response.Meta))

		if response.Status == gemini.StatusSuccess {

			buf := new(bytes.Buffer)
			buf.ReadFrom(response.Body)
			body := buf.String()

			if strings.Contains(response.Meta, "text/") {
				PreviewTextBody(body, "response body")
			} else {
				ShowMessage("\tbody: <some binary " + response.Meta + " was returned>...")
			}

			//optionally save the file
			if *outputPath != "" {
				outBytes := []byte(body + "\n")
				saveFile(outBytes, *outputPath)
			}

			if *extractEdit {
				extractNimigemEdit(body)
			}
		}

		ShowMessage("\n")

	}

}
