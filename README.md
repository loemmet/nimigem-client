# Nimigem client

A basic command line client for Gemini and Nimigem

### Background

Nimigem is a proposed adjacent protocol to Gemini, that fills the gap of non-itempotent content submission.

It is designed to support the development of simple applications that integrate with the client, or allow content upload. 

Nimigem approach:

* client integration
* build on existing Gemini idioms and infrastructure as far as possible
* simple
* non-extensible - payload is a single data:// URI package, no headers
* can be co-hosted with a gemini server

### Nimigem client

Is a simple command line client for Nimigem and Gemini, to assist debugging and development.

Key features:

* Nimigem content upload from a file or stdin
* Preview of request and response to assist debugging
* Ability to save a Gemini resource to a file
* Support client certificates
* Interactive editing of content, for submission

### Building 

```
go build nimigem-client.go
```

